//A continuación, levantemos un servidor Koa rápido.
// Instale Koa:


const koa=require('koa');
const indexRoutes = require('./routes/index');
const movieRoutes = require('./routes/movies');

const app=new koa();

const PORT = process.env.PORT || 1337;

/*app.use(async (ctx)=>{
    ctx.body={
        status:'success',
        message: 'hello, world!'
    };
});*/

app.use(indexRoutes.routes());
app.use(movieRoutes.routes());

const server=app.listen(PORT , ()=>{
    console.log('server listenig on PORT  : ${{PORT}}');
});

module.exports=server;