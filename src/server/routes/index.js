/*
A diferencia de Express, Koa no proporciona ningún middleware 
de enrutamiento. Hay varias opciones disponibles, 
pero usaremos el enrutador koa debido a su simplicidad.*/

const Router=require('koa-router');
const router= new Router();

router.get('/prueba', async (ctx)=>{
    ctx.body={
        status:'success',
        message:'hello, world!'
    };
});

module.exports= router;